<?php
/**
 * Created by PhpStorm.
 * User: kriss
 * Date: 13.2.11
 * Time: 02:47
 */

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php

    //izviedo savienojumu ar datubāzi (serveris, lietotājs, parole, datubāzes nosaukums)
    $db = new mysqli('localhost', 'rs07tracker', 'GfbbfXeKPSbypcWq', 'tracker');

    //pārbauda, vai savienojums ir veiksmīgs, un ja nav, izdrukā kļūdas paziņojumu un kodu
    if ($db->connect_errno) {
        die("Kļūda savienojoties ar MySQL datubāzi: (" . $db->connect_errno . ") " . $db->connect_error);
    }


    $base = 'http://rs07tracker.com';
    $mainCategories = array('add','top','progress','news','donate','statistics','goals','signatures','faq');
    $skills = array('overall', 'att', 'def', 'str', 'hp', 'ranged', 'pray', 'mage', 'cook', 'wc', 'fletch', 'fish', 'fm', 'craft', 'smith', 'mining', 'herb', 'agi', 'thiev', 'slay', 'farm', 'rc', 'hunt', 'con');




    //all progress pages
    $names = $db->query("SELECT * FROM names LIMIT 1500, 1500");
    while ($result = $names->fetch_object())
    {
        $name = $result->rs_name;
        foreach ($skills as $skill)
        {
            echo "<url>
            <loc>$base/progress/$skill/$name</loc>
          </url>";
        }
    }

    ?>


</urlset>
