<?php
include ('./db.php');

$izvelne = mysqli_real_escape_string($db, $_GET['skill']); if (empty($izvelne)){$izvelne = 'begin';}
$id = mysqli_real_escape_string($db, $_GET['name']);


//xp masīvs
$levelxp = array("",0,83,174,276,388,512,650,801,969,1154,1358,1584,1833,2107,2411,2746,3115,3523,3973,4470,5018,5624,6291,7028,7842,8740,9730,10824,12031,13363,14833,16456,18247,20224,22406,24815,27473,30408,33648,37224,41171,45529,50339,55649,61512,67983,75127,83014,91721,101333,111945,123660,136594,150872,166636,184040,203254,224466,247886,273742,302288,333804,368599,407015,449428,496254,547953,605032,668051,737627,814445,899257,992895,1096278,1210421,1336443,1475581,1629200,1798808,1986068,2192818,2421087,2673114,2951373,3258594,3597792,3972294,4385776,4842295,5346332,5902831,6517253,7195629,7944614,8771558,9684577,10692629,11805606,13034431,14391160,15889109,17542976,19368992,21385073,23611006,26068632,28782069,31777943,35085654,38737661,42769801,47221641,52136869,57563718,63555443,70170840,77474828,85539082,94442737,104273167);
$skillset = array(
                        'overall' => 'Overall',
                        'att' => 'Attack',
                        'def' => 'Defence',
                        'str' => 'Strength',
                        'hp' => 'Hitpoints',
                        'ranged' => 'Ranged',
                        'pray' => 'Prayer',
                        'mage' => 'Magic',
                        'cook' => 'Cooking',
                        'wc' => 'Woodcutting',
                        'fletch' => 'Fletching',
                        'fish' => 'Fishing',
                        'fm' => 'Firemaking',
                        'craft' => 'Crafting',
                        'smith' => 'Smithing',
                        'mining' => 'Mining',
                        'herb' => 'Herblore',
                        'agi' => 'Agility',
                        'thiev' => 'Thieving',
                        'slay' => 'Slayer',
                        'farm' => 'Farming',
                        'rc' => 'Runecrafting',
                        'hunt' => 'Hunter',
                        'con' => 'Construction'
  
);

$short = array('overall',
               'att',
               'def',
               'str',
               'hp',
               'ranged',
               'pray',
               'mage',
               'cook',
               'wc',
               'fletch',
               'fish',
               'fm',
               'craft',
               'smith',
               'mining',
               'herb',
               'agi',
               'thiev',
               'slay',
               'farm',
               'rc',
               'hunt',
               'con'
               );



if ($izvelne == 'begin'){
	//DB dala formas aizsūtīšanai.
    if (!empty($_POST['name2'])){
    	$name = mysqli_real_escape_string($db, $_POST['name2']);
    	$goal = mysqli_real_escape_string($db, $_POST['level']);
    	$skill = mysqli_real_escape_string($db, $_POST['skill']);
    	$ip = $_SERVER['REMOTE_ADDR'];
      $check2=$db->query("SELECT * FROM names WHERE rs_name='$name'");
      $x = $check2->num_rows;

      $mas = get_data($name);
        $arr['att'] = $mas[4];
        $arr['def'] = $mas[7];
        $arr['str'] = $mas[10];
        $arr['hp'] = $mas[13];
        $arr['ranged'] = $mas[16];
        $arr['pray'] = $mas[19];
        $arr['mage'] = $mas[22];
        $arr['cook'] = $mas[25];
        $arr['wc'] = $mas[28];
        $arr['fletch'] = $mas[31];
        $arr['fish'] = $mas[34];
        $arr['fm'] = $mas[37];
        $arr['craft'] = $mas[40];
        $arr['smith'] = $mas[43];
        $arr['mining'] = $mas[46];
        $arr['herb'] = $mas[49];
        $arr['agi'] = $mas[52];
        $arr['thiev'] = $mas[55];
        $arr['slay'] = $mas[58];
        $arr['farm'] = $mas[61];
        $arr['rc'] = $mas[64];
        $arr['hunt'] = $mas[67];
        $arr['con'] = $mas[70];


      //check if already exists.
        $m=$db->query("SELECT * FROM goals WHERE name='$name' AND skill='$skill' AND goal_lvl='$goal'");
        $k=$m->num_rows;
        //echo "<h1>$k</h1>";

        if ($goal<=99 && $goal>=1 && in_array($skill, $short) && $x>=1 && $goal>$arr[$skill] && $k==0){
         
          
        $db->query("INSERT INTO `goals` (`id`, `name`, `goal_lvl`, `skill`, `date`, `ip`, `finished`, `finish_date`)
         VALUES (NULL, '$name', '$goal', '$skill', NOW(), '$ip', '0', NULL);");

        $links = $db->query("SELECT * FROM goals WHERE name='$name' AND goal_lvl='$goal' AND skill='$skill' AND ip='$ip'");
        while ($parbaude = $links->fetch_object()){
        	echo '<div class="teksts">Your goal is created. link to it: <a href="/goals/view/' . $parbaude->id . '/"> http://www.rs07tracker.com/goals/view/' . $parbaude->id . '/</a><br>
        	       Or copy it from here and save as bookmark: 
        	                                                     <input type="text" size="40" value="http://www.rs07tracker.com/goals/view/' . $parbaude->id . '/" />
        	                                                  </div>';}
        }
      else{echo '<script>alert("whoooops, Something wasn\'t correct! maybe you tried to add non-existing name.");</script>';}
            
    }


     //HTML forma
if (empty($_POST['name2'])){
	echo '<div class="goal">Add new goal:<br><form method="post" action="">
	         Runescape name: <input type="text" name="name2" id="autocomplete"><br>
	         skill: <select name="skill">
                        
                        <option value="att">Attack</option>
                        <option value="def">Defence</option>
                        <option value="str">Strength</option>
                        <option value="hp">Hitpoints</option>
                        <option value="ranged">Ranged</option>
                        <option value="pray">Prayer</option>
                        <option value="mage">Magic</option>
                        <option value="cook">Cooking</option>
                        <option value="wc">Woodcutting</option>
                        <option value="fletch">Fletching</option>
                        <option value="fish">Fishing</option>
                        <option value="fm">Firemaking</option>
                        <option value="craft">Crafting</option>
                        <option value="smith">Smithing</option>
                        <option value="mining">Mining</option>
                        <option value="herb">Herblore</option>
                        <option value="agi">Agility</option>
                        <option value="thiev">Thieving</option>
                        <option value="slay">Slayer</option>
                        <option value="farm">Farming</option>
                        <option value="rc">Runecrafting</option>
                        <option value="hunt">Hunter</option>
                        <option value="con">Construction</option>
                    </select><br>
             goal level: <select name="level">';
                            $count = 1;
                            for ($count=1; $count <= 99 ; $count++) { 
      	                    echo '<option value="' . $count . '">' . $count . '</option>';
        }


    echo                 '</select>

              <input type="submit" name="Submit" value="Submit" />

	      </form>';//jauna merka pievienosana
  echo '</div>';

              $name4 = mysqli_real_escape_string($db, $_POST['name4']);
      $rez = $db->query("SELECT * FROM goals WHERE name='$name4'");
      $num = mysql_query("SELECT * FROM goals WHERE name='$name4'", $link);
      $skaits = mysql_num_rows($num);

                  
                   echo '<div class="goal"><form method="post" action="">
                         Search name: <input type="text" name="name4" id="autocomplete"/>
                        <input type="submit" name="Submit" value="Submit" />
                       </form>';

                       if (!empty($name4)){
      echo $skaits . ' Results found with name ' . $name4 . ':<br><br>';
      while ($out=$rez->fetch_object()){
        if ($out->finished == 0){echo ' <img src="/img/close_delete.png" width="15" /> ';}
        if ($out->finished == 1){echo ' <img src="/img/check.png" width="15" /> ';}
        echo '<a href="/goals/view/' . $out->id . '/">' . $out->name . ': Level ' . $out->goal_lvl . ' in ' . $skillset[$out->skill] . '. 
              ';

              echo '</a><br>';
          }
      }

     echo '</div>';
	}


	echo '<div class="goal">';
	   $out = $db->query("SELECT * FROM goals ORDER BY date DESC LIMIT 15");
     echo 'Last 15 goals added: <hr>';
     while ($output = $out->fetch_object()){
      echo '<a href="/goals/view/' . $output->id . '/">' . $output->name . ': Level ' . $output->goal_lvl . ' in ' . $skillset[$output->skill] . '.</a><br>';
     }
	echo '</div>';

    echo '<div class="goal">';
     $out = $db->query("SELECT * FROM goals WHERE finished='1' ORDER BY date DESC LIMIT 15");
     echo 'Last 15 goals finished: <hr>';
     while ($output = $out->fetch_object()){
      echo '<a href="/goals/view/' . $output->id . '/">' . $output->name . ': Level ' . $output->goal_lvl . ' in ' . $skillset[$output->skill] . '.</a><br>';
     }
  echo '</div>';
	
}


if ($izvelne == 'view'){ //esosa merka apskatisana
	$goal = $db->query("SELECT * FROM goals WHERE id='$id'");
	  while ($goals=$goal->fetch_object()){
	  	$niks = $goals->name;
	  	$skils = $goals->skill;
	  	$level = $goals->goal_lvl;
	  	$finish = $goals->finished;
	  	$sakums = date($goals->date);
	  	$beigas = date($goals->finish_date);

	  }

    $mas = get_data($niks);
    //pieredzes array
    $xpNow['att'] = $mas[5];
    $xpNow['def'] = $mas[8];
    $xpNow['str'] = $mas[11];
    $xpNow['hp'] = $mas[14];
    $xpNow['ranged'] = $mas[17];
    $xpNow['pray'] = $mas[20];
    $xpNow['mage'] = $mas[23];
    $xpNow['cook'] = $mas[26];
    $xpNow['wc'] = $mas[29];
    $xpNow['fletch'] = $mas[32];
    $xpNow['fish'] = $mas[35];
    $xpNow['fm'] = $mas[38];
    $xpNow['craft'] = $mas[41];
    $xpNow['smith'] = $mas[44];
    $xpNow['mining'] = $mas[47];
    $xpNow['herb'] = $mas[50];
    $xpNow['agi'] = $mas[53];
    $xpNow['thiev'] = $mas[56];
    $xpNow['slay'] = $mas[59];
    $xpNow['farm'] = $mas[62];
    $xpNow['rc'] = $mas[65];
    $xpNow['hunt'] = $mas[68];
    $xpNow['con'] = $mas[71];




      if (empty($beigas)){$beigas = date("Y-m-d H:i:s",time());}
      $pull = $skils . '_xp';
      
      
      //grafiks
      echo '
            <div id="chartdiv" style="margin-top: 2px;"></div>';



      $mas = array();
      $count = 0;  //*********************day
      $output = $db->query("SELECT * FROM stati WHERE name='$niks' AND date BETWEEN '$sakums' AND '$beigas' ORDER BY date ASC ;");
	  $begin = $db->query("SELECT * FROM stati WHERE name='$niks' AND date>='$sakums' LIMIT 1");
      echo '<script>
      $(document).ready(function(){

      
      var xpass = ';
             while ($dati = $output->fetch_object()){


               $div=$count/24;
               $div = round($div, 2);
               $xp = intval($dati->$pull);
               $mas[] = array($div, $xp);
               $sask = 2*$dati->ind;
               $count = $count+$sask;
                    }
                    
        echo json_encode($mas).';';                                            
        
       echo ' var goalass = [[0, ' . $levelxp[$level] . '], [' . ($count-2)/24 . ', ' . $levelxp[$level] . ']];';
      


     echo '
         $.jqplot(\'chartdiv\', [goalass, xpass],

       {     title:\'Goal: level ' . $level . ' in ' . $skillset[$skils] . '\',
       	 series:[ 
                {showMarker:false, color: \'#FF0000\', label:\'Your goal\'},
                {showMarker:false, color: \'#000000\', label:\'Your progress\'},
                     
       	     ],
             legend: { 
             show: true, 
             location: \'se\',
            rendererOptions: {numberColumns: 2}
               }
           });
       });</script>';

     $output2 = $db->query("SELECT * FROM stati WHERE name='$niks' ORDER BY date DESC LIMIT 1;");
     while ($out = $output2->fetch_object()){
     	$gexp = $out->$pull;
     }
	 while ($begexp = $begin->fetch_object()){$sak_xp = $begexp->$pull;}
     $xpdif = $levelxp[$level]-$xpNow[$skils];
     if ($xpdif<0){$xpdif=0;}
	 $totalxp = $levelxp[$level]-$sak_xp;
	 $gainedxp = $xpNow[$skils]-$sak_xp;
	 $percent = $gainedxp/$totalxp*100;
   if ($percent >= '100'){$percent = 100;}
     echo '<div class="teksts">';
     echo 'User: ' . $niks;
     echo '<br>Skill: ' . $skillset[$skils];
     echo '<br>Goal level: ' . $level;
     echo '<br>Goal experience: ' . number_format($levelxp[$level]);
	 echo '<br>Starting experience: ' . number_format($sak_xp);
     echo '<br>Experience now: ' . number_format($xpNow[$skils]);
	 echo '<br>Experience gained: ' . number_format($gainedxp);

	 echo '<br>Percentage done: ' . number_format($percent, 2, '.', ',') . ' %';
     echo '<br>Experience till goal is done: ' . number_format($xpdif);
     echo '<br>Goal added on: ' . $sakums;
     echo '<br>Goal status: ';
         if ($finish == 0){
          if ($xpdif == 0){echo 'Goal finished';}
             else {echo 'Not finished';}}
         if ($finish == 1){echo 'Finished on ' . $beigas;}

     echo '</div>';    

    




}



function get_data($name)
{
$url = 'http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=' . $name;
$ch = curl_init();
$timeout = 5;
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
$data = curl_exec($ch);

curl_close($ch);
$final = preg_replace('/,/', ' ', $data); // iznemam komatus, nahuj ķēpa ar 2 cikliem un '3d masīvu'
$final = preg_replace('/\n/', ' ', $final);  // iznem enterus
$ski = explode(' ', $final); //$ski ir skilu vertibu masivs (man vajag 0-71) 
return $ski;

}



?>

