<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - Bug Fixes</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">
    Hello RS07tracker users,
    After long time I found time and inspiration to fix bugs in TOP and statistics page, probably will add some content soon. Just I have a lot going
    on right now- University, new job, no social life, and anything else, what keeps me busy :)

</div>


<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - Signature contest!</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">Hello all my friends and RS07tracker users.<br> 
	Since we're growing, we have decided, that we have to make something for you, and the first thing, that came in our mind was signatures.<br>
	 Since now I have graphical designer, who can design something great, he is taking care of this business, but there's one little problem. We can't really take pictures from nowhere, and make something great from nothing, that's why, we need your help! Send your best RS07 character pictures, with some sick gears, and your character might be one of signature characters, that will be used, to make great signatures! Take your character pictures now, and send them to<br>
	 <span style="color: orange;">kriss.andrejevs[aet]gmail[dot]com.</span><br> Thanks!
	 <br><br>

	 Everyone who sends picture will be marked in as <span style="color: yellow">helper</span> in statistics page!
</div>




<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - New design</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">Hi RS07tracker users,<br>
	Thanks to Ričards 'worst' Ozoliņš site now has new design, the first one was just concept for beta.
	<br>Race needs just few finish touches and will be in air any moment now.
	<br>Daily xp graph is removed, because it looks like crap. Daily xp gains in numbers will be still available under graphs.
	<br>Changed data collection time- now every 2 hours, because, after data analization- 30 minutes is way too often, 2 hours is absolutely enough.
</div>




<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - Goals is here</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">Hi everyone. I have huge announcement. ability to add your goals is almost finished, just small bug fixes if 
any shows up. Enjoy.<br>
Also added some fixes to graph design, fixed URLs (now they are pretty), put autocomplete for search fields.<br>
autocomplete powered by: <a href="http://jqueryui.com/autocomplete/">http://jqueryui.com/autocomplete/</a><br>
plots powered by: <a href="http://www.jqplot.com/">http://www.jqplot.com/</a>
</div>

<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - TOP 100</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">Added Top 100 feature: shows overall and all skills in 3 tables- daily/weekly/monthly. 
you will be displayed in Top only, if you have gained atleast 1 experience point in skill or overall, and you need to be between 100
highest xp gains.<br>
<br>
Added experience gain statistics in progress tab, under graphs you can see how much xp you have earned in last
24h/week/month. You have to be in tracker for atleast 24 hours to see first table.
</div>


<div class="post_head"><div class="ico Attack_icon_highscores" style="float: left; margin: 4px 5px 0 5px;"></div><h1>RS07tracker - Beta open</h1><div class="author">Posted: <span>Krišs</span></div>
</div>
<div class="teksts">Beta version for xp tracker is here. features available- you can add your nickname to database, and I will start to track your experience gain. after day or so
you can come back and see your first completed graph (daily xp gain). <br> Coming soon- Top 100 and minor fixes/adjustments<br>
don't forget to tell your friends!
</div>
