<?php
include ('./db.php');


if (isset($_GET['skill']))
{
    $skill = mysqli_real_escape_string($db, $_GET['skill']);
}
else
{
    $skill = 'overall';
}

$skillset = array(
    'overall'=> '<a href="/top/overall/"><div class="ico overall"></div></a>',
    'att' => '<a href="/top/att/"><div class="ico Attack_icon_highscores"></div></a>',
    'def' => '<a href="/top/def/"><div class="ico Defence_icon_highscores"></div></a>',
    'str' => '<a href="/top/str/"><div class="ico Strength_icon_highscores"></div></a>',
    'hp' => '<a href="/top/hp/"><div class="ico Hitpoints_icon_highscores"></div></a>',
    'ranged' => '<a href="/top/ranged/"><div class="ico Ranged_icon_highscores"></div></a>',
    'pray' =>	'<a href="/top/pray/"><div class="ico Prayer_icon_highscores"></div></a>',
    'mage' =>	'<a href="/top/mage/"><div class="ico Magic_icon_highscores"></div></a>',
    'cook' => '<a href="/top/cook/"><div class="ico Cooking_icon_highscores"></div></a>',
    'wc' => '<a href="/top/wc/"><div class="ico Woodcutting_icon_highscores"></div></a>',
    'fletch' => '<a href="/top/fletch/"><div class="ico Fletching_icon_highscores"></div></a>',
    'fish' => '<a href="/top/fish/"><div class="ico Fishing_icon_highscores"></div></a>',
    'fm' => '<a href="/top/fm/"><div class="ico Firemaking_icon_highscores"></div></a>',
    'craft' => '<a href="/top/craft/"><div class="ico Crafting_icon_highscores"></div></a>',
    'smith' => '<a href="/top/smith/"><div class="ico Smithing_icon_highscores"></div></a>',
    'mining' => '<a href="/top/mining/"><div class="ico Mining_icon_highscores"></div></a>',
    'herb' => '<a href="/top/herb/"><div class="ico Herblore_icon_highscores"></div></a>',
    'agi' => '<a href="/top/agi/"><div class="ico Agility_icon_highscores"></div></a>',
    'thiev' => '<a href="/top/thiev/"><div class="ico Thieving_icon_highscores"></div></a>',
    'slay' => '<a href="/top/slay/"><div class="ico Slayer_icon_highscores"></div></a>',
    'farm' => '<a href="/top/farm/"><div class="ico Farming_icon_highscores"></div></a>',
    'rc' => '<a href="/top/rc/"><div class="ico Runecrafting_icon_highscores"></div></a>',
    'hunt' => '<a href="/top/hunt/"><div class="ico Hunter_icon_highscores"></div></a>',
    'con' => '<a href="/top/con/"><div class="ico Construction_icon_highscores"></div></a>'
      	);


      echo '<div style="clear:both;"></div><div class="ikonas">';


      foreach ($skillset as $key => $value) {

      		echo '<div class="float-left" >' . $value . '</div>';

      }
     

      echo '</div><div style="clear:both;" >';


   





$pos = 1;
echo '<div class="top">';
echo '<div class="numbers1 top-table">
              Xp gain in last 24 hours: <hr>
           <table>';
$out = $db->query("SELECT * FROM daily_top ORDER BY $skill DESC LIMIT 100");
while ($table = $out->fetch_object()){
	    if (intval($table->$skill) > 0){
		 $niks = str_replace(' ', '-', $table->name);
    echo '<tr>
            <td>'.$pos.'</td>
           <td><a href="/progress/overall/'.$niks.'/">' .$table->name.'</a></td>
            <td>'.number_format($table->$skill, 0, ',', ' ').'</td>
          </tr>';

$pos++;
}
}

echo '</table>
        </div>';


$pos = 1;
echo '<div class="numbers3 top-table">
              Xp gain in last 7 days: <hr>
           <table>';
$out = $db->query("SELECT * FROM weekly_top ORDER BY $skill DESC LIMIT 100");
while ($table = $out->fetch_object()){
	    if (intval($table->$skill) > 0){
		 $niks = str_replace(' ', '-', $table->name);
    echo '<tr>
            <td>'.$pos.'</td>
            <td><a href="/progress/overall/'.$niks.'/">' .$table->name.'</a></td>
            <td>'.number_format($table->$skill, 0, ',', ' ').'</td>
          </tr>';

$pos++;
}
}


echo '</table>
        </div>';

$pos = 1;
echo '<div class="numbers4 top-table">
              Xp gain in last 30 days: <hr>
           <table>';
$out = $db->query("SELECT * FROM monthly_top ORDER BY $skill DESC LIMIT 100");
while ($table = $out->fetch_object()){

    if (intval($table->$skill) > 0){
	 $niks = str_replace(' ', '-', $table->name);
    	echo '<tr>
            <td>'.$pos.'</td>
            <td><a href="/progress/overall/'.$niks.'/">' .$table->name.'</a></td>
            <td>'.number_format($table->$skill, 0, ',', ' ').'</td>
          </tr>';

$pos++;
}
}


echo '</table>
         </div>
        </div></div>';                

?>